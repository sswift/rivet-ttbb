default: Rivet_ATLAS_2018_I1705857.so

.PHONY: test clean

Rivet_ATLAS_2018_I1705857.so: ATLAS_2018_I1705857.cc
	rivet-buildplugin Rivet_ATLAS_2018_I1705857.so ATLAS_2018_I1705857.cc

test: Rivet_ATLAS_2018_I1705857.so
	MAX_EVENTS=10 athena.py RunRivet_test.py

clean: 
	rm *.so
